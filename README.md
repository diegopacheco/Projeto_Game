Projeto 01 de Orientação a Objeto
Diego Pacheco de Azevedo Martins 150123345
Seguem as instruções de execução do Projeto.
1. make clean
2. make
3. make run
Teclas de ação:
a:Move para a esquerda;
s:Move para baixo;
d:Move para diretia;
w:Move para cima;
Importante: não se pode mover na parade, representado por '=', ou seja sera considerado colisão onde o jogador irá ficar parado.
Jogo:
O jogo consiste em um labirinto onde o jogador precisa passar passar pelos obstáculos e desviar dos inimigos, que são representados pelos ícone &. Para ganhar o jogo, o jogador necessita chegar no final do labirinto, representado por 8. Porém para o jogo terminar, o jogador precisa ter colecionado pelo menos 100 pontos. 
