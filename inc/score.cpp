#ifndef TRAP_HPP
#define TRAP_HPP
#include "gameobject.hpp"

    class Score: public GameObject{
    private:
        int ponto;
    public:
        Score();
        Score(int ponto);
        ~Score();
        int getPonto();
        void setPonto(int valor);
        //void movimento();


};
#endif
