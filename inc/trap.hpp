#ifndef TRAP_HPP
#define TRAP_HPP
#include "gameobject.hpp"

    class Trap: public GameObject{
    private:
        int damage;
    public:
        Trap();
        Trap(int damage);
        ~Trap();
        int getDamage();
        void setDamage(int valor);
        //void movimento();


};
#endif
