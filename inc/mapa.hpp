#ifndef MAPA_HPP
#define MAPA_HPP
#include<iostream>
#include<string>

class Mapa{
    private:
        int ant;
        char range[20][50];

    public:
        Mapa();
        void setRange();
        void getRange();
        void Inimigos(int posx, int posy);
        void RetPontos(int posx, int posy);
        void Pontos(int posx, int posy);
        void addElemento(char sprite, int posx, int posy);
        void atualizando(int posx, int posy);
        char getAnt();
        void RetInimigos();

};

#endif
