#ifndef PLAYER_HPP
#define PLAYER_HPP
#include "gameobject.hpp"

    class Player{
    private:
        int alive;
        int score;
        int winner;
    public:
        Player();
        Player(int alive, int score, int winner);
        void setAlive(int alive);
        void setScore(int score);
        void setWinner(int winner);
        int getAlive();
        int getScore();
        int getWinner();
        //void movimento();


};
#endif
