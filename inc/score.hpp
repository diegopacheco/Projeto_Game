#ifndef SCORE_HPP
#define SCORE_HPP
#include "gameobject.hpp"

    class Score: public GameObject{
    private:
        int pontos;
    public:
        Score();
        Score(int pontos);
        ~Score();
        int getPontos();
        void setPontos(int valor);
        //void movimento();


};
#endif
