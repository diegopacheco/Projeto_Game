#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP
#include <iostream>
#include <string>

using namespace std;

class GameObject{
    private:
        int posx;
        int posy;
        char sprite;
        char direcao;

    public:
        GameObject();
        GameObject(char sprite, int posx, int posy);
        char getSprite();
        void setSprite(char sprite);
        int getPosx();
        void setPosx(int valor);
        int getPosy();
        void setPosy(int valor);
        void Movimento();
        void Direcao();

};
#endif
