#include "mapa.hpp"
#include <iostream>
#include <fstream>
#include <ncurses.h>
#include <string>
#include <stdio_ext.h>
#include <ctime>
#include <cstdlib>

using namespace std;

Mapa::Mapa(){}

void Mapa::setRange(){//Pega o arquivo txt do mapa e armazena em um char

    ifstream File("doc/maze_map.txt");
    string aux;
    if(!File){
        cout << "ERRO: OPEN FILE" << endl;
    }
    else{
        for (int i = 0; i < 20; i++) {
            getline(File,aux);
            for (int u = 0; u < 50; u++) {
                range[i][u] = aux[u];
            }
        }
    }
    File.close();
}
void Mapa::getRange(){//Irá mostrar o mapa
    for (int i = 0; i < 20; i++) {
        for (int u = 0; u < 50; u++) {
            printw("%c",range[i][u]);
            if (u >= 49) {
                printw("\n");
            }
        }
    }

}

void Mapa::Inimigos(int posx, int posy){//Adiciona os inimigos aleatoriamente
    srand((unsigned)time(0));
    for (int i = 0; i < 10; i++) {
    do{
        posy = rand()%50;
        posx = rand()%50;
        if (range[posy][posx] == '-' ) {
            range[posy][posx] = '&';
        }
    }while(range[posy][posx] != '&');
    }
}
void Mapa::Pontos(int posx, int posy){//Adiciona os pontos aleatoriamente
    srand((unsigned)time(0));
    for (int i = 0; i < 20; i++) {
    do{
        posy = rand()%50;
        posx = rand()%50;
        if (range[posy][posx] == '-' ) {
            range[posy][posx] = '$';
        }
    }while(range[posy][posx] != '$');
    }
}

void Mapa::addElemento(char sprite, int posx, int posy){//Irá caso bata em um obstaculo ele irá retornar ao simbolo anterior
    if(range[posy][posx]== '='){
        ant = '=';

    }
    else{
        ant = range[posy][posx];
        range[posy][posx] = sprite;

    }

}
void Mapa::atualizando(int posx, int posy){//Atualiza a posição do local onde o jogador estava
    range[posy][posx] = ant;

}
void Mapa::RetPontos(int posx, int posy){//Caso o jogador pegue o ponto irá retornar a '-'
    range[posy][posx] = '-';

}
char Mapa::getAnt(){//Pega a posição do jogador para ser comparada
    return ant;

}

void Mapa::RetInimigos(){//Irá apagar os antigos inimigos, para que possa criar novos

    for (int i = 0; i < 20; i++) {
        for (int j = 0; j < 50; j++) {
            if(range[i][j] == '&')
            range[i][j] = '-';
        }
    }

}
