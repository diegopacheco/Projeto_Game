#include "gameobject.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>


using namespace std;

GameObject::GameObject(){

}

GameObject::GameObject(char sprite, int posx, int posy){
    this->sprite = sprite;
    this->posx = posx;
    this->posy = posy;

}

void GameObject::setPosx(int valor){ //Irá somar a posição x de acordo com o que digitar
    this -> posx += valor;
}
int GameObject::getPosx(){ //Entrega o valor da posição x para quem pedir
    return this->posx;

}
void GameObject::setPosy(int valor){//Irá somar a posição x de acordo com o que digitar
    this -> posy += valor;

}
int GameObject::getPosy(){//Entrega o valor da posição y para quem pedir
    return this->posy;

}
void GameObject::setSprite(char sprite){
    this -> sprite = sprite;

}
char GameObject::getSprite(){
    return this->sprite;

}
void GameObject::Movimento(){//Pedi para onde o jogar irá andar e entrega o valor para ser somado
    direcao = getch();
    if (direcao=='w') {
        this->setPosy(-1);
    }
    else if (direcao == 's') {
        this->setPosy(1);
    }
    else if (direcao == 'a') {
        this->setPosx(-1);
    }
    else if (direcao == 'd') {
        this->setPosx(1);
    }

}
void GameObject::Direcao(){//Caso o jogador ande em um local indevido, ele irá retornar a onde estava
    if (direcao=='w') {
        this->setPosy(1);
    }
    else if (direcao == 's') {
        this->setPosy(-1);
    }
    else if (direcao == 'a') {
        this->setPosx(1);
    }
    else if (direcao == 'd') {
        this->setPosx(-1);
    }

}
