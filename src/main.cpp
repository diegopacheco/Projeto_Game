#include <iostream>
#include <stdlib.h>
#include "player.hpp"
#include "mapa.hpp"
#include "gameobject.hpp"
#include <string>
#include <ncurses.h>
#include <stdio_ext.h>
#include <unistd.h>
#include "trap.hpp"
#include "score.hpp"

using namespace std;

 int main ()
 {
     Mapa *mapa = new Mapa();//Criação do ponteiro da classe mapa
     mapa->setRange();//Irá montar o mapa
     GameObject *player = new GameObject('@', 3, 3);//Criação do ponteiro da classe Game
     //Player *jogador = new Player();
     Trap *trap = new Trap(5);//Criação do ponteiro da classe trap
     Score *score = new Score(0);//Criação do ponteiro da classe Score
     mapa->Pontos(player->getPosx(), player->getPosy());//Adiciona os pontos ao mapa
     int i=0;

        while(TRUE){
            initscr();//Inicia a janela
            clear();//
            //keypad(stdscr, TRUE);
            mapa -> addElemento(player -> getSprite(), player -> getPosx(), player -> getPosy());//Adiciona o jogador no mapa
            if (i==0) {
                mapa->RetInimigos();// Caso o numero de movimentos do jogar chegue a 5, irá apagar as posições do inimigos
            }
            if (i==0) {
                mapa->Inimigos(player -> getPosx(), player -> getPosy());//Qaundo o jogar mover 5 vezes irá criar novas posições dos inimigos
                i=5;
            }
            mapa -> getRange();//Mostrará o mapa
            printw("VIDA:%d\t\t\tSCORE:%d\n",trap->getDamage(),score->getPontos());//Mostrar o tanto de vidas e sua pontução
            printw("@--JOGADOR\t\t&--INIMIGO\t\t$--PONTOS\n");
            mapa -> atualizando(player -> getPosx(), player -> getPosy());//Atualiza a posição de onde o jogador estava
            if(mapa->getAnt()=='-'){
                player -> Movimento();// Caso o jogar não esteja em algum obstaculo, irá continuar movimentando

            }
            else if (mapa->getAnt() == '8' && score->getPontos() == 100) {//Se chegou ao final do jogo, o jogo irá parar
                break;
            }
            else if(mapa->getAnt() == '&'){
                trap->setDamage(1);//Diminui a vida do jogar, case passe por um inimigo
                mapa->RetInimigos();//Apaga os inimigos
                mapa->Inimigos(player-> getPosx(),player->getPosy());//Cria novamento os inimigos
                player-> Movimento();//Jogador movimenta novamente
                refresh();

            }
            else if(mapa->getAnt() == '$'){
                score->setPontos(10);//Adiciona 10 pontos cajo o jogador alcance o ponto
                mapa->RetPontos(player->getPosx(), player->getPosy());//Retira o ponto do mapa
                player-> Movimento();//MOvimento do jogador
            }
            else if (mapa->getAnt() == '='){
                player -> Direcao();//Caso o jogador ande em um obstaculo, irá voltar a posição que estava
            }
            if(trap->getDamage() == 0){//Se o pontos de vida do jogador acabar, o jogo é encerrado
                refresh();
                break;
            }
            i--;
            refresh();//Atualiza o mapa, caso alguma alteração seja feita
            endwin();//Encerra o jogo
    }
     return 0;
 }

/*
struct fichaJogo{
    char mapa[ALT][LAG];
};
struct fichaJogo ficha;

void lerMapa(ifstream& arquivo){
    int i,j;
    if (!arquivo) {
        cout << "Nao deu" << endl;
    }
    while(!arquivo.eof()){
        for (i = 0; i < ALT; i++) {
            for (j = 0; j < LAG; j++) {
                arquivo >> ficha.mapa[i][j];
            }
        }
    }
    arquivo.close();

}

int main ()
{
    int i,j;
    ifstream file("doc/mapa_1.txt",file.in);
    lerMapa(file);
    initscr();
    for (i = 0; i < ALT; i++) {
        for (j = 0; j < LAG; j++) {
            printw("%c",ficha.mapa[i][j]);
        }
        printw("\n");
    }
    getch();
    endwin();
    return 0;
}
*/
